Clone the projet and up the docker-compose file.

$ docker-compose up -d 

check elasticsearch and kibana is up ot not by browser. Now need to install the filebeat to push logs from any server.

$ curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.1.1-amd64.deb
$ sudo dpkg -i filebeat-7.1.1-amd64.deb
$ sudo nano /etc/filebeat/filebeat.yml

add log file path and enable as true. configure kibana and give IP of server. Same configure elasticsearch or logstash with server IP.

Now enable logstash or elasticsearch module

$ sudo filebeat modules enable logstash
or
$ sudo filebeat modules enable elasticsearch

Now check modules for logstash or elasticsearch

$ sudo nano /etc/filebeat/modules.d/logstash.yml
or
$ sudo nano /etc/filebeat/modules.d/elasticsearch.yml

Load configurations by this command

$ sudo filebeat setup
$ sudo service filebeat start

Now check elasticsearch index pattern and create pattern in kibana. Data will be visualize.
